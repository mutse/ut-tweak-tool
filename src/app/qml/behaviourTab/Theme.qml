/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import TweakTool 1.0
import QtQuick.Layouts 1.1

import "../components"
import "../components/ListItems" as ListItems
import "../js/shell.js" as Shell

Page {
    id: rootItem

    header: PageHeader {
        title: i18n.tr("System theme")
        flickable: view.flickableItem
    }

    readonly property var themes: [
        "Ambiance",
        "SuruDark",
        "SuruGradient"
    ]
    property int currentThemeIndex: -1

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            width: view.width

            ListItems.Warning {
                iconName: "security-alert"
                text: "<h4>" + i18n.tr("Unity 8 has three available themes") + "</h4>" +
                    i18n.tr("Currently, only the default theme (Ambiance) is well-supported.") + " " +
                    i18n.tr("The other themes are only useful for testing purposes.")
            }

            ListItems.SectionDivider { id: temp; text: i18n.tr("Theme selection") }

            ListItems.OptionSelector {
                id: selector
                model: [
                    themes[0] + " " + i18n.tr("(default theme)"),
                    themes[1],
                    themes[2]
                ]

                function displayThemeSetting() {
                    var getSystemTheme = Shell.cmdSystemTheme();
                    var currentTheme = Shell.processLaunch(getSystemTheme.cmd).trim();
                    for (var i = 0; i < themes.length; i++) {
                        if (currentTheme === themes[i]) {
                            // Only capture the current theme the first time
                            if (currentThemeIndex === -1) currentThemeIndex = i;
                            selectedIndex = i;
                            break;
                        }
                    }
                }

                Component.onCompleted: {
                    displayThemeSetting();
                }
                onSelectedIndexChanged:  {
                    // Only proceed if a theme was detected and the selection is different to that theme
                    if (currentThemeIndex > -1 && selectedIndex !== currentThemeIndex) {
                        // Only change the theme if Unity restart is accepted
                        var popup = PopupUtils.open(confirmDialog);
                        popup.accepted.connect(function() {
                            var setSystemTheme = Shell.cmdSystemTheme(themes[selectedIndex]);
                            Shell.processLaunch(setSystemTheme.cmd);
                            var restartServiceUnity8 = Shell.cmdServiceUnity8('restart');
                            Shell.processLaunch(restartServiceUnity8.cmd);
                        })
                    }
                }
            }
        }
    }

    Component {
        id: confirmDialog
        Dialog {
            id: confirmDialogue

            signal accepted;

            title: i18n.tr("Restart Unity 8")
            text: i18n.tr("This change requires restarting Unity 8 to take effect.") +
                "<br />" + i18n.tr("Do you want to restart Unity 8 now?") +
                "<br />" + i18n.tr("All currently open apps will be closed.") +
                "<br />" + i18n.tr("Save all your work before continuing!")

            Button{
                text: i18n.tr("Cancel")
                onClicked: {
                    selector.selectedIndex = currentThemeIndex;
                    PopupUtils.close(confirmDialogue);
                }
            }

            Button{
                text: i18n.tr("Restart Unity 8")
                color: theme.palette.normal.negative
                onClicked: {
                    confirmDialogue.accepted();
                    PopupUtils.close(confirmDialogue);
                }
            }
        }
    }
}
